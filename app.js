function calculateValidationNumber(input) {
    if (typeof input == 'number') {
        var _split = input.toString().split('');
        var _total = _split.reduce((a, b) => parseInt(a) + parseInt(b), 0);
        if(_total.toString().length > 1){
            return calculateValidationNumber(_total);
        }
        else{
            return _total;
        }
    }
    else {
        //another output
        //return 0;
        return 'invalid input';
    }
}